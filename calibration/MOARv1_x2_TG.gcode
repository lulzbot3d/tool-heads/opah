; This gcode is formulated to work with dual tool head test stands running TAZ 6 Dual v2 firmware version 1.1.9.9
; Use of this gcode outside of an identical setup is likely to provide unexpected results
T0
M104 S230
T1
M104 S230
M117 FANS 100 PERCENT
M106 P0 S255		     ; set cooling fan 0 to 100%
M106 P1 S255		     ; set cooling fan 1 to 100%

G4 S10			     ; dwell 10 seconds

M117 FANS OFF
M106 P0 S0
M106 P1 S0


G4 S5			     ; dwell 5 seconds


M117 FANS 40 PERCENT
M106 P0 S102		     ; set cooling fan 0 to 40%
M106 P1 S102		     ; set cooling fan 1 to 40%

G4 S10			     ; dwell 10 seconds

M117 FANS OFF
M106 P0 S0
M106 P1 S0


G4 S5			     ; dwell 5 seconds


M999                 ; clear errors
M400                 ; clear buffer
G21                  ; set units to millimeters
M82                  ; use absolute distances for extrusion
T0                   ; tool 0
G92 E0               ; Set cords to zero
T1                   ; tool 1
G92 E0               ; Set cords to zero
M92 T0 E800          ; set T0 esteps to base 800
M92 T1 E800          ; set T1 esteps to base 800
M500                 ; save into memory

T0                   ; select tool 0
M109 S230            ; set extruder nozzle to 230C and wait
G4 S15               ; wait 15 sec to mark extrude and mark filament
G1 E150 F200         ; move extruder 1 300mm
G1 E300 F200
G4 S1

T1                   ; select tool 1
M109 S230            ; set extruder nozzle to 230C and wait
G4 S15               ; wait 15 sec to mark extrude and mark filament
G1 E450 F200         ; move extruder 2 300mm
G1 E600 F200
G4 S1

G4 S1                ; wait

M106 P0 S255         ; turn E0 Fan on 100 percent
M106 P1 S255         ; turn E1 Fan on 100 percent

T0
M104 S0
T1
M109 R60
M106 P0 S0           ; fan off
M106 P1 S0           ; fan off
M84                  ; idle motors
M18                  ; turn off motors
M300 S2
M117 Test Complete

